package com.codercrew.callsmstracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.codercrew.callsmstracker.Globals;

public class LoginActivity extends Activity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        findViewById(R.id.btn_save).setOnClickListener(this);
    }

    private EditText getEdNew(){
        return (EditText) findViewById(R.id.ed_new);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (getEdNew().getText().toString().isEmpty())
            {
                getEdNew().setError("Please enter login password");
                getEdNew().requestFocus();
            }
                else if (getEdNew().getText().toString().equals(Globals.getPassword()))
                {
                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else Toast.makeText(view.getContext(), "Invalid Password", Toast.LENGTH_SHORT).show();

                break;
        }
    }
}
