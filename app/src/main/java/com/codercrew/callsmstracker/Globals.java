package com.codercrew.callsmstracker;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Mehtab Ahmad on 6/14/2016.
 */
public class Globals extends Application
{

    public static SharedPreferences mPreference;
    public static SharedPreferences.Editor mEditor;


    @Override
    public void onCreate() {

        super.onCreate();
        mPreference = getSharedPreferences("my_pref", Context.MODE_PRIVATE);
        mEditor = mPreference.edit();
    }


    public static void setOnnOff(boolean isOn) {
        mEditor.putBoolean("ONN_OFF",isOn);
        mEditor.commit();
    }

    public static boolean getOnnOff() {
       return mPreference.getBoolean("ONN_OFF",true);
    }

    public static void savePassword(String password) {
        mEditor.putString("PASSWORD",password);
        mEditor.commit();
    }
    public static String getPassword() {
        String password = mPreference.getString("PASSWORD",null);
        return password;
    }
    public static void savePhone(String password) {
        mEditor.putString("PHONE",password);
        mEditor.commit();
    }
    public static String getPhone() {
        String password = mPreference.getString("PHONE",null);
        return password;
    }
    public static void saveCode(String password) {
        mEditor.putString("CODE",password);
        mEditor.commit();
    }
    public static String getCode() {
        String code = mPreference.getString("CODE",null);
        return code;
    }
    public static void savePos(int pos) {
        mEditor.putInt("POSITION",pos);
        mEditor.commit();
    }
    public static int getPos() {
        int pos = mPreference.getInt("POSITION",-1);
        return pos;
    }



}
