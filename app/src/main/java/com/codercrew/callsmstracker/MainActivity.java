package com.codercrew.callsmstracker;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends Activity{

	EditText edPhone;
	CheckBox chkStart;
	CheckBox chkStop;
	Button btnSave;
	ImageView imgPass;
	TextView tvCode;
	Spinner CountrySpinner;
	ArrayAdapter<String> countryAdapter;
	String[] CountryNames;
	String[] CountryCodes;
	int countryPos;
	static MainActivity activity;
	int previousSize;

	private AdView mAdView;
	static InterstitialAd mInterstitialAd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		activity = this;
		int permissionCheck = 0;

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
			permissionCheck = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
			if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
				showMessageOKCancel("You need to allow this app for sms and call permissions to deliver you notifications properly. If you deny these permissions then app will not work. If you deny permissions then later you can allow these permissions by going to app settings in phone",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.SEND_SMS}, 100);
							}
						});
			}

			}


		edPhone = (EditText) findViewById(R.id.ed_phone);
		mAdView = (AdView) findViewById(R.id.adView);
		chkStart = (CheckBox) findViewById(R.id.chck_start);
		chkStop =(CheckBox) findViewById(R.id.chck_stop);
		btnSave = (Button) findViewById(R.id.btn_save);
		tvCode = (TextView) findViewById(R.id.tv_code);
		imgPass = (ImageView) findViewById(R.id.img_password);
		CountrySpinner=(Spinner)findViewById(R.id.country_spinner);
		ParseCountryData(CountryDetailClass.CountryJSonData);

		if (Globals.getOnnOff())
		{
			chkStart.setChecked(true);
			chkStop.setChecked(false);
		}
		else
		{
			chkStart.setChecked(false);
			chkStop.setChecked(true);
		}

		if (Globals.getPos()!=-1)
			CountrySpinner.setSelection(Globals.getPos());

		if (Globals.getPhone()!=null)
		{
			edPhone.setText(Globals.getPhone());
			btnSave.setText("Update");
		}
		else
		{
			chkStop.setChecked(true);
			chkStart.setChecked(false);
			Globals.setOnnOff(false);
		}



		chkStart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				chkStart.setChecked(true);
				chkStop.setChecked(false);
				Globals.setOnnOff(true);
			}
		});

		chkStop.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				chkStop.setChecked(true);
				chkStart.setChecked(false);
				Globals.setOnnOff(false);
			}
		});

		btnSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {


				if (edPhone.getText().toString().isEmpty())
				{
					edPhone.setError("Please enter phone number");
				}

				else if (edPhone.getText().toString().substring(0,1).equals("0"))
				{
					warning(MainActivity.this);
				}
				else if (edPhone.getText().toString().length()<6)
					edPhone.setError("Please enter a valid phone number");

				else
				{
					Globals.savePhone(edPhone.getText().toString().trim());
					Globals.saveCode(tvCode.getText().toString().trim());
					Globals.savePos(countryPos);
					btnSave.setText("Update");
					Toast.makeText(MainActivity.this, "Phone number saved", Toast.LENGTH_SHORT).show();
				}

			}
		});

		imgPass.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				ChangePass(MainActivity.this);
			}
		});


		CountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				tvCode.setText(CountryCodes[i]);
				countryPos=i;
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});

		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);

		showAdd(this);

		edPhone.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				previousSize = s.length();

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {

				if (s.length()>0)
				{
					if (previousSize>s.length())
						return;
					String start = s.toString().substring(0,1);
					if (start.equals("0"))
					{
						warning(MainActivity.this);
					}
				}

			}
		});

	}

	public void showAdd(final Context context)
	{

		AdRequest adRequest = new AdRequest.Builder().build();
		mInterstitialAd = new InterstitialAd(context);

		// set the ad unit ID
		mInterstitialAd.setAdUnitId(context.getString(R.string.interstitial));


		// Load ads into Interstitial Ads
		mInterstitialAd.loadAd(adRequest);

	mInterstitialAd.setAdListener(new AdListener() {
			public void onAdLoaded() {
				if (mInterstitialAd.isLoaded()) {
					mInterstitialAd.show();
				}
			}

	});
	}


	public void ChangePass(Activity mActivity) {

		final Dialog dialog = new Dialog(mActivity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.dialog_password);
		DisplayMetrics dm = new DisplayMetrics();
		mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = dialog.getWindow();
		window.setGravity(Gravity.CENTER | Gravity.START);
		lp.copyFrom(window.getAttributes());
		//This makes the dialog take up the full width
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
		final EditText old     = (EditText) dialog.findViewById(R.id.ed_current);
		final EditText newpass = (EditText) dialog.findViewById(R.id.ed_new);
		final EditText confm   = (EditText) dialog.findViewById(R.id.ed_confirm);

		Button btnOK = (Button) dialog.findViewById(R.id.btn_save);
		Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);

		btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dialog.dismiss();
			}
		});
		btnOK.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (old.getText().toString().isEmpty())
				{
					old.setError("Please enter current password");
					old.requestFocus();
				}

				else if (newpass.getText().toString().isEmpty())
				{
					newpass.setError("Please enter current password");
					newpass.requestFocus();
				}

				else if (confm.getText().toString().isEmpty())
				{
					confm.setError("Please confirm new password");
					confm.requestFocus();
				}

				else if (!old.getText().toString().equals(Globals.getPassword()))
				{
					old.setError("Please enter correct current password");
					old.requestFocus();
				}
				else if (!newpass.getText().toString().trim().equals(confm.getText().toString().trim()))
				{
					confm.setError("Password not matched");
					confm.requestFocus();
				}
				else
				{
					Globals.savePassword(newpass.getText().toString().trim());
					dialog.dismiss();
				}


			}
		});
		dialog.show();
	}


	public void warning(Activity mActivity) {

		final Dialog dialog = new Dialog(mActivity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setCancelable(false);
		dialog.setContentView(R.layout.zero_warning);
		DisplayMetrics dm = new DisplayMetrics();
		mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = dialog.getWindow();
		window.setGravity(Gravity.CENTER | Gravity.START);
		lp.copyFrom(window.getAttributes());
		//This makes the dialog take up the full width
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);


		Button btnOK = (Button) dialog.findViewById(R.id.btn_ok);


		btnOK.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	public void ParseCountryData(String data)
	{
		try {
			JSONObject jsonObject=new JSONObject(data);
			JSONArray jsonArray=jsonObject.getJSONArray("countries");
			CountryNames=new String[jsonArray.length()];
			CountryCodes=new String[jsonArray.length()];
			for (int i=0;i<jsonArray.length();i++)
			{

				JSONObject jsonObject1=jsonArray.getJSONObject(i);

				CountryCodes[i]=jsonObject1.getString("code");
				CountryNames[i]=jsonObject1.getString("name");


			}
			countryAdapter=new ArrayAdapter<String>(MainActivity.this,R.layout.simple_spinner_item,CountryNames);
			CountrySpinner.setAdapter(countryAdapter);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		if (mAdView != null) {
			mAdView.pause();
		}
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mAdView != null) {
			mAdView.resume();
		}
	}

	@Override
	public void onDestroy() {
		if (mAdView != null) {
			mAdView.destroy();
		}
		super.onDestroy();
	}


	private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
		new AlertDialog.Builder(MainActivity.this)
				.setMessage(message)
				.setPositiveButton("OK", okListener)
				.setNegativeButton("Cancel", null)
				.create()
				.show();
	}


}

