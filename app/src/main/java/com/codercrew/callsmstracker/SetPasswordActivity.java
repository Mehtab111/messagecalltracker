package com.codercrew.callsmstracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.codercrew.callsmstracker.Globals;
import com.codercrew.callsmstracker.LoginActivity;

public class SetPasswordActivity extends Activity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_password_screen);

        if (Globals.getPassword()!=null)
        {
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
            finish();
        }
        findViewById(R.id.btn_save).setOnClickListener(this);
    }

    private EditText getEdNew(){
        return (EditText) findViewById(R.id.ed_new);
    }

    private EditText getEdConfirm(){
        return (EditText) findViewById(R.id.ed_confirm);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                if (getEdNew().getText().toString().isEmpty()) {
                    getEdNew().setError("Please enter new password");
                    getEdNew().requestFocus();
                } else if (getEdConfirm().getText().toString().isEmpty()) {
                    getEdConfirm().setError("Please confirm new password");
                    getEdConfirm().requestFocus();
                } else if (!getEdConfirm().getText().toString().equals(getEdNew().getText().toString())) {
                    getEdConfirm().setError("Password not matched");
                    getEdConfirm().requestFocus();
                } else {
                    Globals.savePassword(getEdNew().getText().toString());
                    Intent intent = new Intent(view.getContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                break;
        }
    }
}
